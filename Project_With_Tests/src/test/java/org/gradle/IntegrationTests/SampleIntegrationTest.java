package org.gradle.IntegrationTests;

import static org.junit.Assert.assertEquals;

import org.gradle.Person;
import org.junit.Test;

public class SampleIntegrationTest {
	@Test
    public void canConstructAPersonWithAName() {
        Person person = new Person("Larry");
        assertEquals("Larry", person.getName());
    }

}
