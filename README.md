# INTRODUCTION#

This application is helpful for packaging  all tests into jar from Gradle. Projects will be having multiple modules. Multiple people will work on each module in a team. Each and every module may or may not depend on each other.

We will create executable jar for each and every module and share the same to team by maintaining them in some remote repository. So, that every one in the team can access the required jars based on their requirement.

At the time of building if we will get any exception in the project then the build process will be stop. Here difficult to identify the exception in the project because we are using remote jars also.

### OBJECTIVE ###

The objective of this project is to provide facility for testing before using remote jars to find weather their code is perfect or not. 

Developers of particular module/project, They need to provide the tests-jar including executable jars to do testing. Developers who are using remote jar, they need to take  tests-jar(if available) also and they need to test before use.

### SOFTWARE ###

* Java
* Gradle (Building Tool Like Ant and Maven)


### PACKAGING ALL THE TESTS INTO JAR FROM GRADLE ###
Step 1: Here getting all tests from classpath.


```
#!java

task testJar(type: Jar)
 {
     classifier = 'tests'
     from sourceSets.test.output
 }

```

Step 2:Adding tests-jar into artifacts


```
#!java

artifacts
 {
    archives testJar
 }


```
Step 3: Installing all artifacts into local maven repo


```
#!java

gradle install
```